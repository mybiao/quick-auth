import React from 'react'
import {Table} from 'antd'
import axios from '../util/http'

const columns = [
    {
        title: 'id',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: '用户名',
        dataIndex: 'username',
        key: 'username',
    },
    {
        title:"昵称",
        dataIndex: 'nickname',
        key:"nickname"
    },
    
    {
        title: '角色id',
        dataIndex: 'roleId',
        key: 'roleId',
    },
    {
        title: '角色',
        dataIndex: 'roleName',
        key: 'roleName',
    },

]

class UserSetting extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            data: [],
            page:{
                total:0,
                current: 0,
                pageSize:0
            },
            pageSize:0,
            size:2
        }
    }
    componentDidMount = ()=>{
        
        this.getUsers(1,2)
    }

    getUsers=(page,size)=>{
        axios({
            method:"GET",
            url:`/users?page=${page}&size=${size}`
        }).then(res=>{
            if(res.data.code===200){
                let da = res.data.data
                let content = da.content
                for(let d of content){
                    d.key = d.id;
                }
                this.setState({
                    data:content,
                    page:{
                        total: da.totalElements,
                        current: da.number,
                        pageSize: da.size,
                        showTotal: (total,range)=>{
                            return "共"+total+"条"
                        },
                        showTitle: true
                    }
                })
            }
        })
    }

    tableChange = (page,pageSize)=>{
        console.log(page)
        this.getUsers(page,2)
    }

    render(){
        return(
            <div>
                <Table columns={columns} dataSource={this.state.data} 
                
                pagination={{position:["bottomRight"],...this.state.page,onChange:this.tableChange}}></Table>
            </div>
        )
    }
}

export default UserSetting