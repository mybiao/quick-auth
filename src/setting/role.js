import React from 'react'
import {Table,Space,Button} from 'antd'
import axios from '../util/http'

const columns = [
    {
        title:"ID",
        dataIndex:"ID",
        key:"ID"
    },
    {
        title:"角色名",
        dataIndex:"RoleName",
        key:"RoleName"
    },
    {
        title:"操作",
        key:"action",
        render:(text,record)=>(
            <Space size="middle">
                <Button type="link" onClick={e=>viewPermission(record.ID,e)}>查看权限</Button>
            </Space>
        )
    }
]

let viewPermission = (id,e)=>{
    console.log(id)
}

class RoleSetting extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            datas:[]
        }
    }

    componentDidMount = ()=>{
        axios({
            method:"GET",
            url:`/roles?page=1&size=3`,
        }).then(res=>{
            console.log(res.data);
            let dd = res.data.content
            for(let d of dd){
                d.key = d.ID
            }
            this.setState({
                datas:dd
            })
        })
    }

    render(){
        return (
            <div>
                <Table columns={columns} dataSource={this.state.datas}>

                </Table>
            </div>
        )
    }

}

export default RoleSetting