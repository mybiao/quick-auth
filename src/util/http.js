import axios from 'axios'
import {message} from 'antd'

axios.defaults.baseURL = "http://localhost:8090"

axios.interceptors.request.use(config=>{
    if(config.url==="/login"){
        return config
    }
    let token = localStorage.getItem("token")
    if (token==null){
        message.error("请先去登陆！")
        return
    }
    config.headers.authorization = "bearer "+token;
    return config
},err=>{
    return Promise.reject(err);
})

axios.interceptors.response.use(res=>{
    if(res.status===401){
        localStorage.removeItem("token");
        message.error("登陆已过期，请重新登陆!")
    }
    return res;
},err=>{
    message.error("请求错误")
    return Promise.reject(err);
})

export default axios