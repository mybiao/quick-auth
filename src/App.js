import 'antd/dist/antd.css'
import './App.css';
import { Menu, Form, Input, Button } from 'antd';
import { AppstoreOutlined, SettingOutlined } from '@ant-design/icons';
import { Route } from 'react-router-dom'
import UserSetting from './setting/user'
import RoleSetting from './setting/role'
import React from 'react'
import axios from './util/http'

const { SubMenu } = Menu;

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedKeys: [],
      openKeys:[]
    }
  }

  componentDidMount=()=>{
    console.log(this.props.history);
    switch(this.props.history.location.pathname){
      case '/setting_user':
        this.setState({
          selectedKeys:['5'],
          openKeys:['sub2']
        })
        break
      case '/setting_role':
        this.setState({
          selectedKeys:['6'],
          openKeys:['sub2']
        })
        break
        default:
          break
    }
  }

  userSetting = e => {
    this.setState({
      selectedKeys:['5'],
      openKeys:['sub2']
    })
    this.props.history.push('/setting_user');
  }

  toRoleSetting = e=>{
    this.setState({
      selectedKeys:['6'],
      openKeys:['sub2']
    })
    this.props.history.push('/setting_role')
  }

  onFinish=(values)=>{
    let fm = new FormData()
    for (let k in values){
      fm.append(k,values[k])
    }
    axios({
      method:'POST',
      url:"/login",
      data:fm,
      headers: {'content-type':'application/x-www-form-urlencoded'}
    }).then(res=>{
      console.log(res.data)
      const{token} = res.data
      if(token!==undefined && token!==null){
        localStorage.setItem("token",token)
        window.location.reload()
      }
    })
  }

  render() {
    let token = localStorage.getItem("token");
    if (token == null) {
      const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
      };
      const tailLayout = {
        wrapperCol: { offset: 8, span: 16 },
      };
      return (
        <div>
          <Form {...layout} onFinish={this.onFinish}>
            <Form.Item label="用户名" name="username" rules={[{ required: true, message: 'Please input your username!' }]}>
              <Input />
            </Form.Item>
            <Form.Item label="密码" name="password" rules={[{ required: true, message: 'Please input your password!' }]}>
              <Input.Password />
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      )
    }
    return (
      <div className="App">
        <div className="leftClass">
          <Menu
            style={{ width: 256 }}
            defaultSelectedKeys={['5']}
            defaultOpenKeys={['sub2']}
            mode="inline"
          >
            <SubMenu key="sub1" icon={<AppstoreOutlined />} title="Navigation Two">
              <Menu.Item key="1">Option 5</Menu.Item>
              <Menu.Item key="2">Option 6</Menu.Item>
              <SubMenu key="sub3" title="Submenu">
                <Menu.Item key="3">Option 7</Menu.Item>
                <Menu.Item key="4">Option 8</Menu.Item>
              </SubMenu>
            </SubMenu>
            <SubMenu key="sub2" icon={<SettingOutlined />} title="设置">
              <Menu.Item key="5" onClick={this.userSetting} onSelect={this.userSetting}>用户管理</Menu.Item>
              <Menu.Item key="6" onClick={this.toRoleSetting} onSelect={this.toRoleSetting}>角色管理</Menu.Item>
              <Menu.Item key="7">权限管理</Menu.Item>
              <Menu.Item key="8">资源管理</Menu.Item>
            </SubMenu>
          </Menu>
        </div>
        <div className="rightClass">
          <Route path="/setting_user" component={UserSetting} />
          <Route path="/setting_role" component={RoleSetting}/>
        </div>
      </div>
    )
  }
}

export default App;
